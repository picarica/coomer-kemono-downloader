#!/bin/bash

DOWNPARTY="~/c-m-downloader"
COPYTO=(~/adult\ movies/coomerparty)
COOKIES="./cookies.firefox-private.txt"
INCLUDE="('mp4', 'm4v', 'jpg', 'png', 'jpe')"


source ./Models

INITKE=(gallery-dl --cookies $COOKIES --filter "extension in $INCLUDE" --dest "$DOWNPARTY" https://kemono.party/)
INITCO=(gallery-dl --cookies $COOKIES --filter "extension in $INCLUDE" --dest "$DOWNPARTY" https://coomer.party/onlyfans/user/)


#read -p "Stiahnut z listu? <y/N> " prompt
if [[ $1 == list ]]
then
	for MODEL in ${MODELS[@]}; do
		if [[ "$MODEL" == *"|"* ]]
		then
			IFS='|' read -r -a kemono <<< "$MODEL"
			"${INITKE[@]}${kemono[0]}/user/${kemono[1]}"
			
		else
			"${INITCO[@]}$MODEL"
		fi
		# we need these vraibles set so that find command will respect spaces in filenames
		OIFS="$IFS" 
		IFS=$'\n'
		#find and delete all movie files shorter na 420 seconds so 6 Minutes i think
		for file in $(find ~/downs/coomerparty/onlyfans/$MODEL -type f -name "*.m*" | grep -E '.mp4|.m4v' | sort); do
			dur=$(ffprobe -i "$file" -show_entries format=duration -v quiet -of csv="p=0" | cut -f1 -d".")
				if [ "$dur" -gt 30 ]
				then
				mkdir -p ${COPYTO[@]}$MODEL
				rsync -avhP --remove-source-files $file ${COPYTO[@]}$MODEL
				fi
		done 
	done


elif [[ $1 == "custom" ]]; then
	echo "Downloading one-time"
	if [ -n "$2" ]
	then
		DOWNPARTY="$2"
	else
		echo "Missing Path"
		exit
	fi
	echo "Path is set: $DOWNPARTY"

	if [ -n "$4" ]
	then
		COOKIES="$4"
	fi

	INITKE=(gallery-dl --cookies $COOKIES --filter "extension in $INCLUDE" --dest "$DOWNPARTY" https://kemono.party/)
	INITCO=(gallery-dl --cookies $COOKIES --filter "extension in $INCLUDE" --dest $DOWNPARTY https://coomer.party/onlyfans/user/)	

	if [[ "$3" == *"|"* ]] 
	then
		IFS='|' read -r -a kemono <<< "$3"
		"${INITKE[@]}${kemono[0]}/user/${kemono[1]}"
	elif [ -n "$3" ]
	then
		echo "Starting download from coomer.party for $3"
		echo "${INITCO[@]}$3"
	else
		echo "empty ID: Please input user name if on coomer, if on kenomo please set type of service and userID ex. \"patreon|066752\""
		exit
	fi
else 
	echo ""

	echo "if you want to download from List make sure to create file \"Models\" next to down.sh script"
	echo "syntax inside Models file is as array so for example \/"
	echo "Models=("
	echo "	\"somecoomermodel\" #example below"
	echo "	\"belledelphine\" #exact name of model is in the URL"
	echo "	\"service|userID\" # below is an example"
	echo "	\"fanbox|96620\""
	echo ")"
	echo ""
	echo "you can also set permanent download directory in Models file like so:"
	echo "DOWNPARTY=\"~/your/path\""
	echo "and other stuff like cookie file and what type of files will it accept like so"
	echo "COOKIES="./cookies.firefox-private.txt" \#cookie file, you can export them via extension like https://github.com/hrdl-github/cookies-txt "
	echo "INCLUDE=\"('mp4', 'm4v')\""
	echo ""
	echo "if you have that setup you can launch download via:"
	echo "./down.sh list"
	echo ""
	echo "if you want to download one user at a time you can pass"
	echo "options to down.sh, required are save path and user"
	echo ""
	echo "for example:"
	echo "./down.sh custom ~/save-path someuser"
	echo "when downloading from kemono make sure to put user in quote like so"
	echo "\"fanbox|1654\""
fi
