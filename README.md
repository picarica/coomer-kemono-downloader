## coomer-kemono downloader

## About
Galler-dl wrapper written in bash
it helps simplify, list and organize downloads from coomer.party and kemono.party, you can write a list of models, artist and script automatically downloads them, check for existing files.
you can also tell script to only download images or only videos
Soon feature is deleting videos that are shorter than N amount of time

## Getting started

downloader for coomer.party and kemono.party the only dependency is [Gallery-dl](https://github.com/mikf/gallery-dl)
can be downloaded via **pip** 
```
python3 -m pip install -U gallery-dl

```
or from [.bin](https://github.com/mikf/gallery-dl/releases) relase, just don't forget to add it to **$PATH**

then you can either run the script manually and it will download all files into ~/c-m-downloader or you can set your own path

## Usage

if you want to download from List make sure to create file Models next to down.sh script"
syntax inside Models file is as array so for example \/
Models=(
	\"somecoomermodel\" #example below
	\"belledelphine\" #exact name of model is in the URL
	\"service|userID\" # below is an example
	\"fanbox|96620\"
)

if you have that setup you can launch download via:
./down.sh list

if you want to download one user at a time you can pass
options to down.sh, required are save path and user

for example:
./down.sh custom ~/save-path someuser